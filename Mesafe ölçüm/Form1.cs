﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Windows.Forms.DataVisualization.Charting;
//using offis = Microsoft.Office.Interop.Excel;

namespace Mesafe_ölçüm
{
    public partial class Form1 : Form
    {

        string gelen = "0";
        int fireControl = 0;
        int sayac = 1;
        int distanceTimer = 0;
        List<double> distanceList = new List<double>();
        List<double> timeList = new List<double>();
        List<double> ivmeS = new List<double>();
        List<double> kuvvetS = new List<double>();
        Random myRandom = new Random();
        


        int indis;
        DateTime yeni = DateTime.Now;
        int zaman=0;
        int satir=1;
        int sayac2=0;
        int satirNo = 1;
        double F;
        double a;
        int deneme = 1;



        public Form1()
        {
            InitializeComponent();
            label1.Visible = false;
            label4.Visible = false;
          
        }

        

        private void Form1_Load(object sender, EventArgs e)
        {
            serialPort1.Close();
            this.chart1.Titles.Add("Mesafe ölçüm");
            DateTime yeni = DateTime.Now;
             
            
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            timeList.Clear();
            distanceList.Clear();
            label1.Visible = true;
            serialPort1.PortName = "COM7";

            serialPort1.Open();
            timer1.Enabled = true;

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        public void timer1_Tick(object sender, EventArgs e)
        {

            gelen = serialPort1.ReadLine();
            double distance = Convert.ToDouble(gelen) / 10;
            zaman = timer1.Interval+zaman;
            this.chart1.Series["Veri"].Points.AddXY(zaman,distance);
            satir = dataGridView1.Rows.Add();
            dataGridView1.Rows[satir].Cells[0].Value = satirNo;
            dataGridView1.RowsDefaultCellStyle.BackColor = Color.DarkOrange;


            dataGridView1.Rows[satir].Cells[1].Value = gelen;
            dataGridView1.Rows[satir].Cells[2].Value = (sayac*100).ToString();
            dataGridView1.Rows[satir].Cells[3].Value = yeni.ToShortDateString();
           
           
            label1.Text = gelen;
            
            if(fireControl==1)
            {
                distanceList.Add(distance);
                //label2.Text = gelen;
                //label3.Text = sayac.ToString();
                sayac2++;
                distanceTimer = timer2.Interval + distanceTimer;
                timeList.Add(distanceTimer);

            }
            satir++;
            satirNo++;
            sayac++;

        }

        private void aktar_Click(object sender, EventArgs e)
        {

         //   Microsoft.Office.Interop.Excel.Application objExcel = new Microsoft.Office.Interop.Excel.Application();
          //  objExcel.Visible = true;
           // Microsoft.Office.Interop.Excel.Workbook objbook = objExcel.Workbooks.Add(System.Reflection.Missing.Value);
         //   Microsoft.Office.Interop.Excel.Worksheet objSheet = (Microsoft.Office.Interop.Excel.Worksheet)objbook.Worksheets.get_Item(1);
           
            for (int s = 0; s < dataGridView1.Columns.Count; s++ )
            { 
             //   Microsoft.Office.Interop.Excel.Range myrange = (Microsoft.Office.Interop.Excel.Range)objSheet.Cells[1, s+1];
              //  myrange.Value2 = dataGridView1.Columns[s].HeaderText;
            }

            for (int s = 0; s < dataGridView1.Columns.Count; s++)
            {
                for (int j = 0; j < dataGridView1.Rows.Count; j++)
                {
                   // Microsoft.Office.Interop.Excel.Range myrange = (Microsoft.Office.Interop.Excel.Range)objSheet.Cells[j+2, s + 1];
                  //  myrange.Value2 = dataGridView1[s, j].Value;
                 }
            }

            // Microsoft.Office.Interop.Excel.Range objRange;
           
          //  objRange = objSheet.get_Range("A1" ,System.Reflection.Missing.Value);
           // objRange.set_Value(System.Reflection.Missing.Value, dataGridView1.Rows[0].Cells[0].Value);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            chart1.Series[0].Points.Clear();
            dataGridView1.Rows.Clear();
            serialPort1.Close();
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            serialPort1.Close();
            fireControl = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            fireControl = 1;
            timer2.Enabled = true;

            
            
                serialPort1.WriteLine("fire");
            
            
        }

        private void timer2_Tick(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            //timer1.Enabled = false;
           // timer2.Enabled = false;
            label4.Visible = true;
            // dizideki max bulma işlemleri negatif ivmeyi engellemek için...
            double maxRange = distanceList[0];           
            int countofList = distanceList.Count;
            int i;
            for(i=1;i<countofList;i++)
            {
                if(maxRange<distanceList[i])
                {
                    maxRange = distanceList[i];
                    indis = i;
                }
                
            }
            while(maxRange!=distanceList[distanceList.Count-1])
            {
                distanceList.RemoveAt(distanceList.Count - 1);
                timeList.RemoveAt(distanceList.Count - 1);

            }
            for(i=0;i<distanceList.Count;i++)
            {
                this.chart2.Series["İvme"].Points.AddXY(timeList[i].ToString(), distanceList[i].ToString());

            }

            double speed1 = 0;
            
            double substitution1 = 0;
            double substitution2 = 0;
            double acceleration = 0;


            substitution1 = distanceList[0];
            substitution2 = distanceList[distanceList.Count - 1];
            speed1 = (substitution1 - substitution2) / ((timeList[0] - timeList[distanceList.Count - 1])/1000);
            acceleration = speed1 / timeList[timeList.Count - 1];
            
           label4.Text=Math.Round(acceleration,3).ToString();
            a = Math.Round(acceleration,3);
            ivmeS.Add(a);

         

      

        }

        private void button4_Click(object sender, EventArgs e)
        {
            F = Math.Round((a * Convert.ToDouble(textBox1.Text.ToString())/100),3);
           label3.Text = Math.Round(F,3).ToString();
            this.chart3.Series["Kuvvet"].Points.AddXY(deneme++,Math.Round(F,3).ToString());
            kuvvetS.Add(F);

        }
    }
}
